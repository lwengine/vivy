package com.alexeyglukharev.features.search.domain

import android.location.Location
import com.alexeyglukharev.common.data.location.LocationRepository
import com.alexeyglukharev.features.search.data.DoctorsRepository
import com.alexeyglukharev.features.search.data.entities.DoctorsDTO
import com.alexeyglukharev.features.search.domain.entities.DoctorVO
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import org.amshove.kluent.`should be`
import org.junit.Before
import kotlin.test.Test

private const val LAT = 52.534709
private const val LNG = 13.3976972

class SearchResultUseCaseImplTest {

    val doctorsRepository: DoctorsRepository = mockk()
    val locationRepository: LocationRepository = mockk()
    val location: Location = mockk()
    lateinit var useCase: SearchResultUseCaseImpl

    @Before
    fun setup() {
        val dto = mockk<DoctorsDTO>()
        val doctor = mockk<DoctorsDTO.Doctor>()
        every { dto.lastKey } returns "key"
        every { dto.doctors } returns listOf(doctor)
        every { doctor.id } returns "id"
        every { doctor.name } returns "doctor name"
        every { doctor.address } returns "address"

        every { locationRepository.getLastLocation() } returns location
        every { location.latitude } returns LAT
        every { location.longitude } returns LNG
        every {
            doctorsRepository.fetchDoctorList(
                search = any(),
                lastKey = any(),
                location = any()
            )
        } returns Observable.just(dto)

        useCase = SearchResultUseCaseImpl(doctorsRepository, locationRepository)
    }

    @Test
    fun `update last key if there are some doctros in the responce`() {
        val dto = mockk<DoctorsDTO>()
        val doctor = mockk<DoctorsDTO.Doctor>()
        every { dto.lastKey } returns "key"
        every { dto.doctors } returns listOf(doctor)

        useCase.updateLastKey(dto)

        useCase.lastKey.get() `should be` "key"
    }

    @Test
    fun `(edge case) update last key if doctors list is empty`() {
        val dto = mockk<DoctorsDTO>()
        every { dto.lastKey } returns "key"
        every { dto.doctors } returns emptyList()

        useCase.updateLastKey(dto)

        useCase.lastKey.get() `should be` null
    }

    @Test
    fun `(edge case) update last key if last key is null`() {
        val dto = mockk<DoctorsDTO>()
        val doctor = mockk<DoctorsDTO.Doctor>()
        every { dto.lastKey } returns null
        every { dto.doctors } returns listOf(doctor)

        useCase.updateLastKey(dto)

        useCase.lastKey.get() `should be` null
    }

    @Test
    fun `search for a doctor`() {
        val query = "test"

        useCase.searchDoctors(query)

        verify {
            doctorsRepository.fetchDoctorList(
                search = query,
                lastKey = null,
                location = location
            )
        }
    }

    @Test
    fun `load next page`() {
        useCase.lastKey.set("key")

        useCase.loadNextPage()

        verify {
            doctorsRepository.fetchDoctorList(
                search = null,
                lastKey = "key",
                location = null
            )
        }
    }

    @Test
    fun `search for a doctor in case we have a last key`() {
        val query = "test"
        useCase.lastKey.set("old key")

        useCase.searchDoctors(query)

        verify {
            doctorsRepository.fetchDoctorList(
                search = query,
                lastKey = null,
                location = location
            )
        }
    }

    @Test
    fun `load next page when we've started loading`() {
        useCase.lastKey.set("key")

        useCase.isLoading.get() `should be` false

        useCase.loadNextPage()
            .test()
            .assertOf { !useCase.isLoading.get() }
            .dispose()

        useCase.isLoading.get() `should be` false

        verify {
            doctorsRepository.fetchDoctorList(
                search = null,
                lastKey = "key",
                location = null
            )
        }
    }

    @Test
    fun `convert DTO to DO`() {
        val dto = mockk<DoctorsDTO>()
        val doctor = mockk<DoctorsDTO.Doctor>()
        every { dto.lastKey } returns "key"
        every { dto.doctors } returns listOf(doctor)
        every { doctor.id } returns "id"
        every { doctor.name } returns "doctor name"
        every { doctor.address } returns "address"
        val doctorVO = DoctorVO(
            id = doctor.id.hashCode().toLong(),
            name = doctor.name,
            address = doctor.address
        )

        useCase.run {
            Observable.just<DoctorsDTO>(dto)
                .convert()
                .test()
                .assertValue(doctorVO)
                .dispose()
        }

        useCase.isLoading.get() `should be` false
    }

    @Test
    fun `convert DTO to DO with empty doctros list`() {
        val dto = mockk<DoctorsDTO>()
        every { dto.lastKey } returns null
        every { dto.doctors } returns emptyList()

        useCase.run {
            Observable.just<DoctorsDTO>(dto)
                .convert()
                .test()
                .assertValueCount(0)
                .dispose()
        }

        useCase.isLoading.get() `should be` false
    }
}