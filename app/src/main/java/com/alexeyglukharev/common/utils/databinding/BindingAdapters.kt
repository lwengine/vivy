package com.alexeyglukharev.common.utils.databinding

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("show")
fun View.showOrGoneView(show: Boolean?) {
    when (show) {
        true -> visibility = View.VISIBLE
        false -> visibility = View.GONE
    }
}