package com.alexeyglukharev.common.utils.extensions

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun CoroutineScope.launchDelayed(delay: Long, body: (() -> Unit)) =
    launch(Dispatchers.Main) {
        kotlinx.coroutines.delay(delay)
        body.invoke()
    }