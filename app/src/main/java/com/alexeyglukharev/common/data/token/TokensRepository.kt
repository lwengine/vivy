package com.alexeyglukharev.common.data.token

import javax.inject.Inject

interface TokenRepository {
    fun getToken(): String?
    fun storeToken(token: String?)
    fun hasToken(): Boolean
    fun clearToken()
}

class TokenRepositoryImpl @Inject constructor() : TokenRepository {
    private var token: String? = null

    override fun getToken(): String? = token

    override fun storeToken(token: String?) {
        this.token = token
    }

    override fun clearToken() {
        token = null
    }

    override fun hasToken() = !token.isNullOrEmpty()
}