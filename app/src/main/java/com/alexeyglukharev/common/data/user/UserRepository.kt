package com.alexeyglukharev.common.data.user

import javax.inject.Inject

interface UserRepository {
    fun getUserName(): String
    fun getPassword(): String
}

class UserRepositoryImpl @Inject constructor() : UserRepository {

    override fun getUserName() = "androidChallenge@vivy.com"

    // TODO shame to us if we are going to keep it this way :)
    override fun getPassword() = "88888888"
}