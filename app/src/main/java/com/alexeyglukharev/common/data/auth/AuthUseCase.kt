package com.alexeyglukharev.common.data.auth

import com.alexeyglukharev.common.data.auth.AuthDataSource.Companion.OAUTH_ENDPOINT
import com.alexeyglukharev.common.data.token.TokenRepository
import com.alexeyglukharev.common.data.user.UserRepository
import io.reactivex.schedulers.Schedulers
import okhttp3.FormBody
import javax.inject.Inject

interface AuthUseCase {
    fun refreshToken()
    fun isAuthorized(): Boolean
    fun isOauthEndpoint(url: String): Boolean
    fun getToken(): String?
}

class AuthUseCaseImpl @Inject constructor(
    private val dataSource: AuthDataSource,
    private val userRepository: UserRepository,
    private val tokenRepository: TokenRepository
) : AuthUseCase {

    /**
     * Refresh token synchronically
     */
    override fun refreshToken() {
        tokenRepository.clearToken()

        val requestBody = FormBody.Builder()
            .add(AuthDataSource.USER_NAME, userRepository.getUserName())
            .add(AuthDataSource.PASSWORD, userRepository.getPassword())
            .build()

        dataSource.auth(
            body = requestBody
        )
            .subscribeOn(Schedulers.io())
            .blockingFirst()
            .let { data ->
                tokenRepository.storeToken(data.accessToken)
            }
    }

    override fun isAuthorized() = tokenRepository.hasToken() // TODO check token expired date

    override fun isOauthEndpoint(url: String) = url == OAUTH_ENDPOINT

    override fun getToken() = tokenRepository.getToken()
}