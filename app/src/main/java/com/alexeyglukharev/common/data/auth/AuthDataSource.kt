package com.alexeyglukharev.common.data.auth

import com.alexeyglukharev.BuildConfig
import com.alexeyglukharev.common.data.auth.entities.AuthDTO
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface AuthDataSource {

    companion object {
        const val OAUTH_ENDPOINT = "https://auth.staging.vivy.com/oauth/token?grant_type=password"

        const val USER_NAME = "username"
        const val PASSWORD = "password"
    }

    @POST(OAUTH_ENDPOINT)
    @Headers(
        "Content-Type: application/x-www-form-urlencoded",
        "Accept: application/json",
        "Authorization: Basic ${BuildConfig.AUTH_BASIC}"
    )
    fun auth(
        @Body body: RequestBody
    ): Observable<AuthDTO>
}

