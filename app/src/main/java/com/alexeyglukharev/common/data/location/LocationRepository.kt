package com.alexeyglukharev.common.data.location

import android.location.Location
import javax.inject.Inject

interface LocationRepository {

    fun getLastLocation(): Location
}

class LocationRepositoryImpl @Inject constructor() : LocationRepository {
    override fun getLastLocation() = Location("").apply {
        latitude = 52.534709
        longitude = 13.3976972
    }
}