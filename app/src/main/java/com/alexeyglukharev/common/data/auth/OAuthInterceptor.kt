package com.alexeyglukharev.common.data.auth

import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import javax.inject.Provider

private const val AUTHORIZATION_HEADER_KEY = "Authorization"
private const val AUTHORIZATION_HEADER_BEARER = "Bearer"

class OAuthInterceptor(
    private val authUseCase: Provider<AuthUseCase>
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response? {
        val response = executeMainRequest(chain)
        val requestUrl = response.request().url().toString()
        return when {
            response.code() == 401 && authUseCase.get().isOauthEndpoint(requestUrl) -> response
            response.code() == 401 -> {
                Timber.d("request a new token. url: $requestUrl")
                authUseCase.get().refreshToken()
                executeMainRequest(chain)
            }
            else -> response
        }
    }

    private fun executeMainRequest(chain: Interceptor.Chain): Response =
        chain.request().newBuilder()
            .apply {
                if (authUseCase.get().isAuthorized()) {
                    header(
                        AUTHORIZATION_HEADER_KEY,
                        "$AUTHORIZATION_HEADER_BEARER ${authUseCase.get().getToken()}"
                    )
                }
            }
            .build()
            .run { chain.proceed(this) }
}