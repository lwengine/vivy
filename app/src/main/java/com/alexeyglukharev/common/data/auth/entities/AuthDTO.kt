package com.alexeyglukharev.common.data.auth.entities

import com.google.gson.annotations.SerializedName

data class AuthDTO(
    @SerializedName("access_token")
    val accessToken: String?
)