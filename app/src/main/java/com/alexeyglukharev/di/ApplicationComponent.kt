package com.alexeyglukharev.di

import android.content.Context
import com.alexeyglukharev.VivyApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivitiesModule::class,
        NetworkModule::class,
        DataSourcesModule::class,
        CommonDomainLayerModule::class,
        CommonDataLayerModule::class
    ]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: VivyApplication)
}