package com.alexeyglukharev.di

import com.alexeyglukharev.features.search.di.SearchModule
import com.alexeyglukharev.features.search.di.SearchScope
import com.alexeyglukharev.features.search.presentation.SearchActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    @SearchScope
    @ContributesAndroidInjector(modules = [SearchModule::class])
    abstract fun injectSearchActivity() : SearchActivity
}