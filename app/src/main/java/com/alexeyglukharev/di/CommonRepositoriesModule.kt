package com.alexeyglukharev.di

import com.alexeyglukharev.common.data.auth.AuthUseCase
import com.alexeyglukharev.common.data.auth.AuthUseCaseImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface CommonDomainLayerModule {

    @Binds
    @Singleton
    fun bindAuthUseCase(impl: AuthUseCaseImpl): AuthUseCase

}