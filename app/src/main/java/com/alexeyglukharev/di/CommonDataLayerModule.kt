package com.alexeyglukharev.di

import com.alexeyglukharev.common.data.location.LocationRepository
import com.alexeyglukharev.common.data.location.LocationRepositoryImpl
import com.alexeyglukharev.common.data.token.TokenRepository
import com.alexeyglukharev.common.data.token.TokenRepositoryImpl
import com.alexeyglukharev.common.data.user.UserRepository
import com.alexeyglukharev.common.data.user.UserRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface CommonDataLayerModule {

    @Binds
    @Singleton
    fun bindTokenRepository(impl: TokenRepositoryImpl): TokenRepository

    @Binds
    @Singleton
    fun bindUserRepository(impl: UserRepositoryImpl): UserRepository

    @Binds
    @Singleton
    fun bindLocationRepository(impl: LocationRepositoryImpl): LocationRepository
}