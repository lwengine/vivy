package com.alexeyglukharev.di

import com.alexeyglukharev.common.data.auth.AuthDataSource
import com.alexeyglukharev.features.search.data.UsersDataSource
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class DataSourcesModule {

    @Provides
    @Singleton
    fun provideUsersDataSource(retrofit: Retrofit): UsersDataSource = retrofit.create(
        UsersDataSource::class.java
    )

    @Provides
    @Singleton
    fun provideAuthDataSource(retrofit: Retrofit): AuthDataSource =
        retrofit.create(AuthDataSource::class.java)

}