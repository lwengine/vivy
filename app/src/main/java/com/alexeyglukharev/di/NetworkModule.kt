package com.alexeyglukharev.di

import com.alexeyglukharev.BuildConfig
import com.alexeyglukharev.common.data.auth.AuthUseCase
import com.alexeyglukharev.common.data.auth.OAuthInterceptor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import timber.log.Timber
import java.io.IOException
import javax.inject.Provider
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        private const val BASE_URL = "https://api.staging.vivy.com/api/"

        private const val HEADER_CONTENT_ENCODING = "Content-Encoding"
        private const val GZIP = "gzip"

        const val HEADER_AUTHORIZATION = "Authorization"
        const val BEARER = "Bearer"
    }

    @Provides
    @Singleton
    fun createGson() = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        .create()

    @Provides
    @Singleton
    fun provideRetrofit(
        gson: Gson,
        authRepository: Provider<AuthUseCase>
    ) = Retrofit
        .Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .client(createClient(authRepository))
        .build()


    private fun createClient(
        authRepository: Provider<AuthUseCase>
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(OAuthInterceptor(authRepository))
            .addInterceptor(addAuthHeader(authRepository))
            .addInterceptor(gzipRequestInterceptor())
            .addInterceptor(log())
            .build()
    }

    private fun gzipRequestInterceptor() = object : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val originalRequest = chain.request()
            if (originalRequest.header(HEADER_CONTENT_ENCODING) != null) {
                return chain.proceed(originalRequest)
            }

            val compressedRequest = originalRequest.newBuilder()
                .header(HEADER_CONTENT_ENCODING, GZIP)
                .method(originalRequest.method(), originalRequest.body())
                .build()
            return chain.proceed(compressedRequest)
        }
    }

    private fun log(): Interceptor {
        val logger = HttpLoggingInterceptor.Logger { message ->
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Logger.DEFAULT.log(message)
            Timber.d(message)
        }

        val logInterceptor = HttpLoggingInterceptor(logger)
        logInterceptor.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.HEADERS

        return logInterceptor
    }

    private fun addAuthHeader(authRepository: Provider<AuthUseCase>) = Interceptor { chain ->
        var request = chain.request()
        authRepository.get().getToken()?.let { token ->
            if (request.header(HEADER_AUTHORIZATION) == null) {
                request = chain.request()
                    .newBuilder()
                    .addHeader(HEADER_AUTHORIZATION, "$BEARER $token")
                    .build()
            }
        }
        chain.proceed(request)
    }
}