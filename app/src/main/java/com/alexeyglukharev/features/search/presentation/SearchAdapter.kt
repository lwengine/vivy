package com.alexeyglukharev.features.search.presentation

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.alexeyglukharev.R
import com.alexeyglukharev.common.utils.VivyAsyncListDiffer
import com.alexeyglukharev.databinding.ItemSearchDoctorBinding
import com.alexeyglukharev.features.search.domain.entities.DoctorVO
import com.alexeyglukharev.features.search.domain.entities.LoadingVO
import com.alexeyglukharev.features.search.domain.entities.SearchVO

private const val TYPE_ITEM = 10
private const val TYPE_LOADING = 20

class SearchAdapter(asyncListDifferStatusCallback: VivyAsyncListDiffer.AsynListDifferStatusCallback? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var scrolledToTheBottomListener: (() -> Unit)? = null

    init {
        setHasStableIds(true)
    }

    private val asyncListDiffer =
        VivyAsyncListDiffer(this, DifferItemCallback()).apply {
            statusCallback = asyncListDifferStatusCallback
        }

    fun submitList(list: List<SearchVO>?) {
        asyncListDiffer.submitList(list, silent = false)
    }

    override fun getItemCount(): Int = asyncListDiffer.currentList.size

    override fun getItemId(position: Int): Long = getItem(position).id

    private fun getItem(position: Int) = asyncListDiffer.currentList[position]

    override fun getItemViewType(position: Int): Int =
        when (getItem(position)?.type) {
            SearchVO.SearchItemType.ITEM -> TYPE_ITEM
            else -> TYPE_LOADING
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            TYPE_ITEM -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_search_doctor, parent, false)
                DoctorItemViewHolder(view)
            }
            TYPE_LOADING -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_loading, parent, false)
                LoadingViewHolder(view)
            }
            else -> throw Exception("unsupproted type $viewType")
        }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val type = getItemViewType(position)
        when (getItemViewType(position)) {
            TYPE_ITEM -> {
                (getItem(position) as? DoctorVO)
                    ?.let { item -> (holder as? DoctorItemViewHolder)?.bind(item) }
            }
            TYPE_LOADING -> {
                scrolledToTheBottomListener?.invoke()
            }
            else -> throw Exception("unsupproted type $type")
        }
    }

    private inner class DifferItemCallback : DiffUtil.ItemCallback<SearchVO>() {
        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: SearchVO, newItem: SearchVO): Boolean {
            return when (oldItem) {
                is DoctorVO -> oldItem == (newItem as? DoctorVO)
                is LoadingVO -> oldItem == newItem
                else -> false
            }
        }

        override fun areItemsTheSame(oldItem: SearchVO, newItem: SearchVO): Boolean {
            return oldItem.type == newItem.type && oldItem::class == newItem::class
        }
    }

    inner class DoctorItemViewHolder(containerView: View) : RecyclerView.ViewHolder(containerView) {

        private val binding: ItemSearchDoctorBinding? = DataBindingUtil.bind(containerView)

        fun bind(item: DoctorVO) {
            binding?.item = item
            binding?.executePendingBindings()
        }
    }

    inner class LoadingViewHolder(containerView: View) :
        RecyclerView.ViewHolder(containerView)
}