package com.alexeyglukharev.features.search.presentation

import android.os.Parcelable
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alexeyglukharev.R
import com.alexeyglukharev.common.utils.VivyAsyncListDiffer
import com.alexeyglukharev.databinding.ActivitySearchBinding
import com.alexeyglukharev.features.search.domain.entities.SearchVO
import com.ethanhua.skeleton.Skeleton
import com.ethanhua.skeleton.SkeletonScreen
import javax.inject.Inject

private const val SKELETON_COUNT = 15

interface SearchView {
    fun submitList(list: List<SearchVO>)
    fun showLoading()
    fun hideLoading()
}

class SearchViewImpl @Inject constructor(
    activity: AppCompatActivity,
    private val binding: ActivitySearchBinding,
    private val viewModel: SearchViewModel
) : SearchView {

    private val listAdapter: SearchAdapter by lazy { SearchAdapter(ListDifferStatusCallback()) }

    private val skeleton: SkeletonScreen

    init {
        with(activity) {
            activity.setSupportActionBar(binding.toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        with(binding.list) {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

            layoutAnimation =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_show)

            adapter = listAdapter
        }

        skeleton = Skeleton.bind(binding.list)
            .adapter(listAdapter)
            .load(R.layout.item_search_skeleton)
            .frozen(true)
            .count(ResourcesCompat.getColor(activity.resources, R.color.shimmer, activity.theme))
            .count(SKELETON_COUNT)
            .show()

        listAdapter.scrolledToTheBottomListener = { viewModel.scrolledToBottom() }
    }

    override fun submitList(list: List<SearchVO>) {
        listAdapter.submitList(list)
    }

    override fun showLoading() {
        skeleton.show()
    }

    override fun hideLoading() {
        skeleton.hide()
    }

    private inner class ListDifferStatusCallback :
        VivyAsyncListDiffer.AsynListDifferStatusCallback {

        var savedState: Parcelable? = null

        override fun onDiffStarted() {
            savedState = binding.list.layoutManager?.onSaveInstanceState()
        }

        override fun onDiffFinished() {
            binding.list.layoutManager?.onRestoreInstanceState(savedState)
        }
    }
}