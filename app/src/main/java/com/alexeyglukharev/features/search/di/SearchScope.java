package com.alexeyglukharev.features.search.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;


// but we don't really need it yet
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface SearchScope {
}
