package com.alexeyglukharev.features.search.di

import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.alexeyglukharev.R
import com.alexeyglukharev.common.utils.extensions.viewModel
import com.alexeyglukharev.databinding.ActivitySearchBinding
import com.alexeyglukharev.features.search.data.DoctorsRepository
import com.alexeyglukharev.features.search.data.DoctorsRepositoryImpl
import com.alexeyglukharev.features.search.domain.SearchResultUseCase
import com.alexeyglukharev.features.search.domain.SearchResultUseCaseImpl
import com.alexeyglukharev.features.search.presentation.*
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
abstract class SearchModule {

    @Module
    companion object {

        @Provides
        @Reusable
        @JvmStatic
        fun provideBindings(activity: AppCompatActivity): ActivitySearchBinding =
            DataBindingUtil.setContentView(activity, R.layout.activity_search)

        @Provides
        @Reusable
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            searchResultUseCase: SearchResultUseCase
        ): SearchViewModel =
            activity.viewModel { SearchViewModelImpl(searchResultUseCase) }
    }

    @Binds
    abstract fun bindActivity(impl: SearchActivity): AppCompatActivity

    @Binds
    @Reusable
    abstract fun bindDoctorsRepository(impl: DoctorsRepositoryImpl): DoctorsRepository

    @Binds
    @Reusable
    abstract fun binSearchView(impl: SearchViewImpl): SearchView

    @Binds
    @Reusable
    abstract fun binSearchResultUseCase(impl: SearchResultUseCaseImpl): SearchResultUseCase

    @Binds
    @Reusable
    abstract fun binSearchBinder(impl: SearchBinderImpl): SearchBinder
}