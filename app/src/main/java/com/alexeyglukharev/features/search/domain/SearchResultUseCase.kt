package com.alexeyglukharev.features.search.domain

import androidx.annotation.VisibleForTesting
import com.alexeyglukharev.common.data.location.LocationRepository
import com.alexeyglukharev.features.search.data.DoctorsRepository
import com.alexeyglukharev.features.search.data.entities.DoctorsDTO
import com.alexeyglukharev.features.search.domain.entities.DoctorVO
import io.reactivex.Observable
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference
import javax.inject.Inject

interface SearchResultUseCase {
    fun searchDoctors(search: String): Observable<DoctorVO>
    fun loadNextPage(): Observable<DoctorVO>
    fun hasNextPage(): Boolean
}

class SearchResultUseCaseImpl @Inject constructor(
    private val doctorsRepository: DoctorsRepository,
    private val locationRepository: LocationRepository
) : SearchResultUseCase {

    @VisibleForTesting
    val lastKey = AtomicReference<String?>()
    @VisibleForTesting
    val isLoading = AtomicBoolean(false)

    override fun searchDoctors(search: String): Observable<DoctorVO> {
        lastKey.set(null)
        isLoading.set(true)
        return doctorsRepository.fetchDoctorList(
            search = search,
            location = locationRepository.getLastLocation(),
            lastKey = lastKey.get()
        ).convert()
    }

    override fun loadNextPage(): Observable<DoctorVO> =
        if (!isLoading.getAndSet(true)) {
            doctorsRepository.fetchDoctorList(
                lastKey = lastKey.get()
            ).convert()
        } else {
            Observable.empty<DoctorVO>()
        }

    override fun hasNextPage() = !lastKey.get().isNullOrEmpty()

    @VisibleForTesting
    fun Observable<DoctorsDTO>.convert(): Observable<DoctorVO> =
        switchMap {
            updateLastKey(it)
            Observable.fromIterable(it.doctors)
        }
            .map { dto ->
                DoctorVO(
                    id = dto.id.hashCode().toLong(),
                    name = dto.name,
                    address = dto.address
                )
            }
            .doFinally { isLoading.set(false) }

    @VisibleForTesting
    fun updateLastKey(it: DoctorsDTO) {
        if (!it.doctors.isNullOrEmpty()) {
            lastKey.set(it.lastKey)
        } else {
            lastKey.set(null)
        }
    }
}