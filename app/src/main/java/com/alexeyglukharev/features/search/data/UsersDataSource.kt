package com.alexeyglukharev.features.search.data

import com.alexeyglukharev.features.search.data.entities.DoctorsDTO
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface UsersDataSource {
    @GET("users/me/doctors")
    fun fetchDoctors(
        @Query("search") search: String?,
        @Query("lat") lat: Double?,
        @Query("lng") lng: Double?,
        @Query("lastKey") lastKey: String?
    ): Observable<DoctorsDTO>
}