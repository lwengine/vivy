package com.alexeyglukharev.features.search.presentation

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alexeyglukharev.common.utils.extensions.launchDelayed
import com.alexeyglukharev.common.utils.extensions.onValueChange
import com.alexeyglukharev.features.search.domain.SearchResultUseCase
import com.alexeyglukharev.features.search.domain.entities.DoctorVO
import com.alexeyglukharev.features.search.domain.entities.LoadingVO
import com.alexeyglukharev.features.search.domain.entities.SearchVO
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Job

private const val TEXT_CHANGE_DEBOUNCE_TIME = 500L

abstract class SearchViewModel : ViewModel() {

    // Fields
    abstract val searchText: ObservableField<String>
    abstract val showError: ObservableField<Boolean>
    abstract val showNotFound: ObservableField<Boolean>

    // Event
    abstract val showLoading: LiveData<Boolean>
    abstract val items: LiveData<List<SearchVO>>

    // Direct actions
    abstract fun scrolledToBottom()
}

class SearchViewModelImpl(
    private val searchResultUseCase: SearchResultUseCase
) : SearchViewModel() {

    // Fields
    override val searchText = ObservableField<String>("")
    override val showError = ObservableField<Boolean>(false)
    override val showNotFound = ObservableField<Boolean>(false)

    // Event
    override val showLoading = MutableLiveData<Boolean>()
    override val items = MutableLiveData<List<SearchVO>>()

    private val compositeDisposable = CompositeDisposable()

    init {
        // handle search text
        var searchDebounce: Job? = null
        searchText.onValueChange { searchQuery ->
            searchDebounce?.cancel()
            searchDebounce = viewModelScope.launchDelayed(TEXT_CHANGE_DEBOUNCE_TIME) {
                search(searchQuery)
            }
        }

        // initial search request
        search("")
    }

    override fun scrolledToBottom() {
        loadMore()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    private fun search(searchQuery: String) {
        searchResultUseCase.searchDoctors(searchQuery)
            .toList()
            .addLoadingItemIfNeeded()
            .doOnSubscribe { showLoadingState() }
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { list -> handleListChanges(list) },
                { showErrorState() }
            )
            .also { compositeDisposable.add(it) }
    }


    private fun loadMore() {
        Observable.fromIterable(items.value)
            .filter { it is DoctorVO }
            .map { it as DoctorVO }
            .concatWith(searchResultUseCase.loadNextPage())
            .toList()
            .addLoadingItemIfNeeded()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { list ->
                items.value = list
            }
            .also { compositeDisposable.add(it) }
    }

    private fun Single<List<DoctorVO>>.addLoadingItemIfNeeded() =
        this.map { list ->
            // add "load more" item to the list if needed
            if (searchResultUseCase.hasNextPage()) {
                return@map list.plus(LoadingVO(id = list.hashCode().toLong()))
            }
            return@map list
        }

    private fun showLoadingState() {
        items.value = emptyList()
        showLoading.value = true
        showError.set(false)
        showNotFound.set(false)
    }

    private fun showErrorState() {
        items.value = emptyList()
        showLoading.value = false
        showError.set(true)
        showNotFound.set(false)
    }

    private fun showNotFoundState() {
        items.value = emptyList()
        showLoading.value = false
        showError.set(false)
        showNotFound.set(true)
    }

    private fun updateDoctorList(list: List<SearchVO>) {
        items.value = list
        showLoading.value = false
        showError.set(false)
        showNotFound.set(false)
    }

    private fun handleListChanges(list: List<SearchVO>) {
        if (list.isEmpty()) {
            showNotFoundState()
        } else {
            updateDoctorList(list)
        }
    }
}