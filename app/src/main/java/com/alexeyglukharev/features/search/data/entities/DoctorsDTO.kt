package com.alexeyglukharev.features.search.data.entities

data class DoctorsDTO(
    val doctors: List<Doctor>,
    val lastKey: String?
) {
    data class Doctor(
        val id: String,
        val address: String,
        val email: String?,
        val highlighted: Boolean,
        val integration: String,
        val lat: Double,
        val lng: Double,
        val name: String,
        val openingHours: List<String>,
        val phoneNumber: String,
        val photoId: String?,
        val rating: Double,
        val reviewCount: Int,
        val source: String,
        val specialityIds: List<Int>,
        val translation: Translation?,
        val website: String
    ) {
        data class Translation(
            val confidentialityKey: String,
            val consentKey: String,
            val requestSubmittedKey: String
        )
    }
}