package com.alexeyglukharev.features.search.domain.entities

interface SearchVO {
    val id: Long
    val type: SearchItemType

    enum class SearchItemType {
        ITEM, LOADING
    }
}

data class DoctorVO(
    override val id: Long,
    val name: String,
    val address: String,
    override val type: SearchVO.SearchItemType = SearchVO.SearchItemType.ITEM
) : SearchVO

data class LoadingVO(
    override val id: Long,
    override val type: SearchVO.SearchItemType = SearchVO.SearchItemType.LOADING
) : SearchVO

