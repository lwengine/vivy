package com.alexeyglukharev.features.search.data

import android.location.Location
import com.alexeyglukharev.features.search.data.entities.DoctorsDTO
import io.reactivex.Observable
import javax.inject.Inject

interface DoctorsRepository {

    fun fetchDoctorList(
        search: String? = null,
        lastKey: String? = null,
        location: Location? = null
    ): Observable<DoctorsDTO>
}

class DoctorsRepositoryImpl @Inject constructor(private val dataSource: UsersDataSource) :
    DoctorsRepository {

    override fun fetchDoctorList(
        search: String?,
        lastKey: String?,
        location: Location?
    ): Observable<DoctorsDTO> =
        dataSource.fetchDoctors(
            search = search,
            lat = location?.latitude,
            lng = location?.longitude,
            lastKey = lastKey
        )
}