package com.alexeyglukharev.features.search.presentation

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.alexeyglukharev.databinding.ActivitySearchBinding
import javax.inject.Inject

interface SearchBinder {

    fun bind(owner: LifecycleOwner)
}

class SearchBinderImpl @Inject constructor(
    binding: ActivitySearchBinding,
    private val viewModel: SearchViewModel,
    private val view: SearchView
) : SearchBinder {

    init {
        binding.viewModel = viewModel
    }

    override fun bind(owner: LifecycleOwner) {
        viewModel.showLoading.observe(owner,
            Observer { if (it) view.showLoading() else view.hideLoading() }
        )

        viewModel.items.observe(owner, Observer { view.submitList(it) })
    }
}