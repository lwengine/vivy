package com.alexeyglukharev

import android.app.Activity
import android.app.Application
import com.alexeyglukharev.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class VivyApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        DaggerApplicationComponent
            .builder()
            .context(this)
            .build()
            .apply {
                inject(this@VivyApplication)
            }
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector
}